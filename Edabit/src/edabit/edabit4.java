package edabit;

public class edabit4 {
public class Challenge {
	public static int[] amplify(int num) {
	int [] result = new int [num];
        for (int i = 1; i <= num; i++){
            if (i % 4 == 0){
                result[i-1] = i * 10;
            } else {
                result[i-1] = i;
            }
        }
        return result;
    }
}
}